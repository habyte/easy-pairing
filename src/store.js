import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

import router from './router'

const API_URL = 'https://easy-pairing-api.herokuapp.com'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    team: {},
    authorizationToken: '',
    errorMessage: ''
  },
  mutations: {
    saveToken (state, token) {
      localStorage.setItem('authorizationToken', token)
      state.authorizationToken = token
      state.errorMessage = ''
    },
    saveTeam (state, team) {
      localStorage.setItem('team', JSON.stringify(team))
      state.team = team
    },
    logout () {
      localStorage.removeItem('authorizationToken')
      localStorage.removeItem('team')
      router.push('/')
    },
    setErrorMessage (state, message) {
      state.errorMessage = message
    }
  },
  actions: {
    async createTeam (_, payload) {
      await axios.post(API_URL + '/teams', payload)
    },
    async authenticateTeam ({ commit }, payload) {
      const authentication = await axios.post(API_URL + '/authentication', payload)
      commit('saveToken', authentication.data.token)

      const team = await axios.get(API_URL + '/teams/' + authentication.data.team_id, {
        headers: {
          'Authorization': authentication.data.token
        }
      })
      commit('saveTeam', team.data)

      router.push('/team')
    },
    async renameTeam ({ commit }, payload) {
      const token = localStorage.getItem('authorizationToken') || this.state.authorizationToken
      const team = this.state.team.id ? this.state.team : JSON.parse(localStorage.getItem('team'))

      await axios.put(
        API_URL + '/teams/' + team.id, payload,
        { headers: { 'Authorization': token } }
      )

      const updatedTeam = await axios.get(API_URL + '/teams/' + team.id, {
        headers: {
          'Authorization': token
        }
      })

      commit('saveTeam', updatedTeam.data)
    },
    async deleteMember ({ commit }, memberId) {
      const token = localStorage.getItem('authorizationToken') || this.state.authorizationToken
      const team = this.state.team.id ? this.state.team : JSON.parse(localStorage.getItem('team'))

      await axios.delete(
        API_URL + '/members/' + memberId,
        { headers: { 'Authorization': token } }
      )

      const updatedTeam = await axios.get(API_URL + '/teams/' + team.id, {
        headers: {
          'Authorization': token
        }
      })

      commit('saveTeam', updatedTeam.data)
    },
    async addMember ({ commit }, payload) {
      const token = localStorage.getItem('authorizationToken') || this.state.authorizationToken
      const team = this.state.team.id ? this.state.team : JSON.parse(localStorage.getItem('team'))

      await axios.post(
        API_URL + '/teams/' + team.id + '/members', payload,
        { headers: { 'Authorization': token } }
      )

      const updatedTeam = await axios.get(API_URL + '/teams/' + team.id, {
        headers: {
          'Authorization': token
        }
      })

      commit('saveTeam', updatedTeam.data)
    },
    async addPairingRecord ({ commit }, payload) {
      const token = localStorage.getItem('authorizationToken') || this.state.authorizationToken
      const team = this.state.team.id ? this.state.team : JSON.parse(localStorage.getItem('team'))

      await axios.post(
        API_URL + '/pairing_records', payload,
        { headers: { 'Authorization': token } }
      )

      const updatedTeam = await axios.get(API_URL + '/teams/' + team.id, {
        headers: {
          'Authorization': token
        }
      })

      commit('saveTeam', updatedTeam.data)
    },
    setErrorMessage({ commit }, error) {
      commit('setErrorMessage', error)
    }
  }
})
