import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Login from './views/Login.vue'
import Team from './views/Team.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      beforeEnter: (to, from, next) => {
        if (localStorage.getItem('authorizationToken')) {
          return next('team')
        }
        next()
      }
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      beforeEnter: (to, from, next) => {
        if (localStorage.getItem('authorizationToken')) {
          return next('team')
        }
        next()
      }
    },
    {
      path: '/team',
      name: 'team',
      component: Team,
      beforeEnter: (to, from, next) => {
        if (localStorage.getItem('authorizationToken')) {
          return next()
        }
        next('login')
      }
    }
  ]
})
