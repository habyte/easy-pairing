# EasyPairing

An open-source project which builds matrixes to help agile teams with pair programming.

Some of its features are:

- Generating pairing matrixes for any team
- Recommending pairs based on how many times each member paired with the others
- Keeping track of the pairing dates

## Looking for the back-end?
[https://github.com/hscorrea/easy-pairing-api](https://github.com/hscorrea/easy-pairing-api)

## Running the project

### Install dependencies
```
npm install
```

### Compile and enable hot-reloading for development
```
npm run serve
```
